<?php
    require_once("Conexao.class.php");
    require_once("../Modelo/PM.class.php");
    final class PC{
        public function acharTodos(){
            $conexao = new Conexao("../Conf/confi.ini");
            //COMANDO SQL PARA SELECIONAR OS DADOS
            $sql = "SELECT * FROM texto;";
            $comando = $conexao->getConexao()->prepare($sql);
            //executa o comando sql
            $comando->execute();
            $resu = $comando->fetchAll();
            //faz a varredura do array
            $list = array();
            foreach($resu as $item){
                $post = new Post();
                $post->setId($item->id);
                $post->setFoto($item->foto);
                $post->setTipo($item->tipo);
                $post->setTit($item->tit);
                $post->setTxt($item->txt);
                array_push($list, $post);
            }
            $conexao->__destruct();
            return $list;
        }

        public function consulta_id($id){
            $conexao = new Conexao("../Conf/confi.ini");
            //COMANDO SQL PARA SELECIONAR OS DADOS
            $sql = "SELECT tipo, foto FROM texto WHERE id=:id";
            $comando = $conexao->getConexao()->prepare($sql);
            $comando->bindValue(":id", $id);
            //executa o comando sql
            $comando->execute();
            $var = $comando->fetch();
            //faz a varredura do array
            $post = new Post();
            $post->setFoto($var->foto);
            $post->setTipo($var->tipo);  
            $conexao->__destruct();
            return $post;
        }

        public function adicionarPost($post){
            //faz a conexao
            $conexao = new Conexao("../Conf/confi.ini");
            //COMANDO SQL PARA INSERIR OS DADOS
            $sql = "INSERT INTO texto(tit, txt, foto, tipo) VALUES (:ti,:te,:fi,:tipo)";
            //prepara para ser modificada pelo php
            $comando = $conexao->getConexao()->prepare($sql);
            //substitue os valores de referencia para os valores das variaveis do livro
            $comando->bindValue(":ti",$post->getTit());
            $comando->bindValue(":te",$post->getTxt());
            $comando->bindValue(":fi",file_get_contents($post->getFoto()['tmp_name']));
            $comando->bindValue(":tipo",$post->getTipo());
            //executa o comando sql
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
        public function atualizaPost($post){

            $conexao = new Conexao("../Conf/confi.ini");
            $up = $conexao->getConexao()->prepare("UPDATE texto SET 
            tit =:titulo, txt = :texto,
            foto = :foto, tipo = :tipo   WHERE id=:id");
            $up->bindValue(":titulo", $post->getTit());
            $up->bindValue(":texto", $post->getTxt());
            $up->bindValue(":id", $post->getId());
            $up->bindValue(":foto",file_get_contents($post->getFoto()['tmp_name']));
            $up->bindValue(":tipo",$post->getTipo());
            
            if($up->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
        public function deletaPost($id){
            $conexao = new Conexao("../Conf/confi.ini");
            //deleta livro
            $del = $conexao->getConexao()->prepare("DELETE FROM texto WHERE id=:id"); 
            $del->bindValue(":id",$id);
            if($del->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
        public function selecionarId($id){
            $conexao = new Conexao("../Conf/confi.ini");
            $sql = "SELECT * FROM texto WHERE id=:id";
            $comando = $conexao->getConexao()->prepare($sql);
            $comando->bindValue(":id", $id);
            $comando->execute();
            $resu = $comando->fetch();
            $post = new Post();
            $post->setId($resu->id);
            $post->setTit($resu->tit);
            $post->setTxt($resu->txt);
            $post->setFoto($resu->foto);
            $post->setTipo($resu->tipo);
            $conexao->__destruct();
            return $post;
        }

    }


?>