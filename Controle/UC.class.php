<?php
    require_once("Conexao.class.php");
    require_once("../modelo/UM.class.php");
    final class UC{
        public function acharTodos(){
            $conexao = new Conexao("../Conf/confi.ini");
            //COMANDO SQL PARA SELECIONAR OS DADOS
            $sql = "SELECT * from usuarios ";
            $comando = $conexao->getConexao()->prepare($sql);
            //executa o comando sql
            $comando->execute();
            $resu = $comando->fetchAll();
            //faz a varredura do array
            $lista = array();
            foreach($resu as $item){
                $login = new User();
                $login->setId($item->id);
                $login->setSenha($item->senha);
                $login->setEmail($item->email);
                $login->setUser($item->user);
                array_push($lista, $login);
            }
            $conexao->__destruct();
            return $lista;
        }

        public function adicionarU($login){
            //faz a conexao
            $conexao = new Conexao("../Conf/confi.ini");
            //COMANDO SQL PARA INSERIR OS DADOS
            $sql = "INSERT INTO usuarios(nUser, email, senha) VALUES (:us,:em,:se)";
            //prepara para ser modificada pelo php
            $comando = $conexao->getConexao()->prepare($sql);
            //substitue os valores de referencia para os valores das variaveis do livro
            $comando->bindValue(":us",$login->getUser());
            $comando->bindValue(":em",$login->getEmail());
            $comando->bindValue(":se",$login->getSenha());
            //executa o comando sql
            if($comando->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
        public function atualizaU($user){

            $conexao = new Conexao("../Conf/confi.ini");

            $up = $conexao->getConexao()->prepare("UPDATE usuarios SET 
            nUser =:user, email =:email, senha = :senha 
            WHERE id=:id");

            $up->bindValue(":user", $user->getUser());
            $up->bindValue(":senha", $user->getSenha());
            $up->bindValue(":id", $user->getId());
            
            if($up->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
        public function aniquilarU($id){
            $conexao = new Conexao("../Conf/confi.ini");
            //deleta livro
            $del = $conexao->getConexao()->prepare("DELETE FROM usuarios WHERE id=:id");
            $del->bindValue(":id",$id);
            if($del->execute()){
                $conexao->__destruct();
                return true;
            }else{
                $conexao->__destruct();
                return false;
            }
        }
        public function verificarLogin($user,$senha){
            $conexao = new Conexao("../Conf/confi.ini"); 
            //seleciona pelo nome         
            $sql = "SELECT * FROM usuarios WHERE nUser=:nome ";
            $comando = $conexao->getConexao()->prepare($sql);
            $comando->bindValue(":nome", "$user");
            $comando->execute();
            if($comando->rowCount() == 1 ){
                $resu = $comando->fetch(PDO::FETCH_OBJ);
                $cipher = "AES-128-CBC";
                $ivlen = openssl_cipher_iv_length($cipher);
                $secret_iv = md5($senha);
                $iv = substr(hash('sha256',$secret_iv),0, 16);
                $key = file_get_contents("C:/Users/clara/Documents/fotosfamilia/WebContent_resources_ByBpSs_key.txt");
                $ciphertext = openssl_decrypt(base64_decode($resu->senha),$cipher,$key, 0, $iv);   
                    if($ciphertext == $senha){
                        return (int)$resu->id;
                    }else{
                        return false;
                    }  
                }else{
                    return false;
                }
        
                
            $conexao->__destruct();
        }
    }
?>