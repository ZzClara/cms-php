<?php
    try{
        require_once("../Controle/PC.class.php");
        $executa = new PC();
        $post = new Post();
        $post->setId($_POST['id']);
        $post->setTxt($_POST['txt']);
        $post->setTit($_POST['tit']);
        $post->setFoto($_FILES['fileT']);
        $post->setTipo($_FILES['fileT']['type']);

        if($executa->atualizaPost($post)){
            session_start();
            $_SESSION['erro'] = "deu certo";
            header("Location: Visu.php");
        }else{
            throw new Exception("Erro ao inserir.");
        }
    }catch(Exception $e){
        session_start();
        $_SESSION['erro'] = $e->getMessage();
        header("Location: index.php");        
    }
?>