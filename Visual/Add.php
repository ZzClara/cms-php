<?php
    try{
        session_start();
        require_once("../Controle/PC.class.php");
        $executa = new PC();
        $post = new Post();
        $post->setTxt($_POST['txt']); 
        $post->setFoto($_FILES['fileT']);
        $post->setTipo($_FILES['fileT']['type']);
        $post->setTit($_POST['tit']);

        if($executa->adicionarPost($post)){
            $_SESSION['erro'] ;
            header("Location: Viagens.php");
        }else{
            header("Location: PagI.php");
            throw new Exception("Erro ao inserir.");
        }
    }catch(Exception $e){
        session_start();
        $_SESSION['erro'] = $e->getMessage();
        header("Location: PagI.php");        
    }
?>