<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel='stylesheet' type='text/css' href='css/bootstrap.min.css'>
    <link rel='stylesheet' type='text/css' href='css/Viag.css'>
    <title>Post</title>
</head>
<body>
    <?php
        require_once("../Controle/PC.class.php");
        $comando = new PC();
        session_start();
        if(!empty($_POST['tit'])){
            $comando->atualizaPost($_GET['id'], $_POST['tit'], $_POST['txt']);
            header("Location: Visu.php");
        }
        if(!isset($_SESSION['user'])){
            header("Location: PagI.php");
        }
    ?>
        <div class='container'>
        
            <div class='d-flex justify-content-center h-100'>
                <div class='card'>
                    <div class='card-header'>
                        <div id="form">
                            <form action="Att.php" method="post" enctype="multipart/form-data">
                            <?php $resu = $comando->selecionarId($_GET['id'])?>
                                <h3>Adicionar Post</h3>
                                <input type="hidden" name="id" value="<?php echo "{$resu->getId()}"?>" id="id" />
                                <label for="tit">Titulo</label>
                                <input type="text" name="tit" value="<?php echo "{$resu->getTit()}"?>" id="titulo" /></br>
                                <label for="fileT">Foto</label>
                                
                                <td><img src='VerFt.php?id=<?php echo "{$resu->getId()}"?>' width='100' height='100' id="fotoimg"/></td>

                                <input type="file" name="fileT" id="fileT" onchange="alterImg()">
                                <label for="txt">Texto</label>
                                <textarea type="text" name="txt"  id="texto" ><?php echo "{$resu->getTxt()}" ?></textarea>
                                <input type="submit" name="enviar" value="atualizar"/>
                            </form>
                        </div>
                    </div>
                     <a href='Visu.php' class='btn btn-secondary btn-lg active' role='button' aria-pressed='' id=''>Voltar</a>
                   
                </div>
            </div>
        </div>
    <script type="text/javascript">
        function alterImg(){
            fileRead(document.getElementById("fileT")).then(response => {
                document.getElementById("fotoimg").src = response;
            });
        }

        fileRead = (input) => {
            return new Promise(function(resolver, reject){
                if(input.files && input.files[0]){
                    var reader = new FileReader();
                    reader.onload = function(e){
                        resolver(e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }else{
                    reject("");
                }
            });
        }

    </script>
</body>
</html>