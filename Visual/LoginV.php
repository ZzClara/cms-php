<?php
session_start();
if(!isset($_SESSION['user'])){
echo"
	<!DOCTYPE html>
	<html lang='pt-br'>

	<head>
        <link href='css/bootstrap.min.css' rel='stylesheet' id='bootstrap-css' />
        <link href='css/sweetalert2.min.css' rel='stylesheet' type='text/css' />
        <link rel='stylesheet' type='text/css' href='css/Log.css'>
        <link rel='stylesheet' type='text/css' href='css/ma.css'>
			<link #sheet' href='https://use.fontawesome.com/releases/v5.3.1/css/all.css' integrity='sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU' crossorigin='anonymous'>-->
			<script src='js/jquery.js' type='text/javascript'></script>
	</head>
	<body>

		<div class='container'>
		
			<div class='d-flex justify-content-center h-100'>
		<div class='card'>
			<div class='card-header'>
				<h3>FAÇA SEU LOGIN</h3>
		
			</div>
			<div class='card-body'>
				<form id='form' method='post' action='#'>
					<div class='input-group form-group' >
						<div class='input-group-prepend'>
							<span class='input-group-text'><i class='fas fa-user'></i></span>
						</div>
						<input type='text' class='form-control' placeholder='Usuário' name='nome' required>
						
					</div>
					<div class='input-group form-group'>
						<div class='input-group-prepend'>
							<span class='input-group-text'><i class='fas fa-key'></i></span>
						</div>
						<input type='password' class='form-control' placeholder='Senha' name='senha' required>
					</div>

					<div class='form-group botoes'>
						<div class='loginbotao'>
						<input type='submit' value='Login' id='log' class='btn float-right login_btn' onclick='return verificarUser();' />
						</div>
						<div class='voltar'>
						<a href='PagI.php' class='btn float-left login_btn ' role='button' aria-pressed='true' id=''>Voltar</a>
						</div>
					</div>
				</form>
			</div>
			<div class='card-footer'>
				<div class='d-flex justify-content-center links'>
					Ainda não é cadastrado?<a href='Cadastro.php'>Cadastre-se</a>
				</div>
			</div>
		</div>
	</div>
		</div>
	<script src='js/jquery.js'></script>
    <script src='js/bootstrap.min.js'></script>
    <script src='js/ai.js'></script>
     <script type='text/javascript' src='js/swettAlert.min.js' ></script>
	<script src='js/script.js' type='text/javascript'></script>
	</body>
	</html>
";
}else{
	header("Location: Visu.php");
}
?>