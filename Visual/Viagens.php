<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel='stylesheet' type='text/css' href='css/bootstrap.min.css'>
    <link rel='stylesheet' type='text/css' href='css/Viag.css'>

    <title>Publicacoes</title>
</head>
<body>
    <?php
        session_start();
        if(!isset($_SESSION['user'])){
            header("Location: PagI.php");
        }
        require_once("../Controle/PC.class.php");
        $comando = new PC();
    ?>
    <div class='container'>
        
        <div class='d-flex justify-content-center h-100'>
            <div class='card'>
                <div class='card-header'>
                    <div id="form">
                        <form action="Add.php" method="post" enctype="multipart/form-data">
                            <h3>Adicionar Publicações</h3>
                            <label for="tit">Titulo</label>
                            <input type="text" name="tit" id="titulo" /><br /> <br />
                            <label for="fileT">Arquivo</label>
                            <input type="file" name="fileT" id="foto" /><br /> <br />
                            <label for="txt">Texto</label>
                            <textarea type="text" name="txt"  id="texto" ></textarea><br /> <br />
                            <input type="submit" name="enviar" value="enviar"  />
                        </form>
                    </div>
                </div>
      <a href='Visu.php' class='btn btn-secondary btn-lg active' role='button' aria-pressed='' id=''>Voltar</a>
            </div>
        </div>
    </div>
</body>
</html>